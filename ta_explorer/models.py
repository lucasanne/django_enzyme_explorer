
from django.db import models


class Transaminases(models.Model):
    name = models.CharField(max_length=200)
    uniprot_id = models.CharField(max_length=200)

