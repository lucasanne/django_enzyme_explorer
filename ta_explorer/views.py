from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Transaminases

from django.http import HttpResponse
from django.views.generic.list import ListView
from ta_explorer.models import Transaminases

def index(request):
    return HttpResponse("Hello, Lucas.")

class IndexView(generic.ListView):
    model = Transaminases
    #template_name = 'ta_explorer/index.html'
    context_object_name = 'ta_list'
    queryset = Transaminases.objects.all()

class DetailView(generic.DetailView):
    model = Transaminases
    template_name = 'ta_explorer/detail.html'

